#!/bin/bash

SCRIPT_RELATIVE_DIR=$(dirname "${BASH_SOURCE[0]}")
SD_DEVICE_DEFAULT="/dev/mmcblk0"
DEVICE_NAME_DEFAULT="pclcd-zynq0"
IMAGE_NAME=caribou-image
IMAGE_PATH="sdimage.direct"
CREATE_IMAGE="n"

#OS specific setup
if [[ "$OSTYPE" == "darwin"* ]]; then
    BLOCK_SIZE="1m"
else
    BLOCK_SIZE="1M"
fi

#Colours
ORANGE='\033[0;33m'
GREEN='\033[0;32m'
RED="\033[0;31m"
BLUE="\033[0;34m"
NC='\033[0m'

#Copy image to the SD card
read -p "Please enter absolute path SD card device [$SD_DEVICE_DEFAULT]: " SD_DEVICE
SD_DEVICE=${SD_DEVICE:-$SD_DEVICE_DEFAULT}

# Get device name
read -p "Please enter the target device name from list: pclcd-lab-zynq, pclcd-testbeam-zynq, pclcd-zynqX [$DEVICE_NAME_DEFAULT]: " DEVICE_NAME
DEVICE_NAME=${DEVICE_NAME:-$DEVICE_NAME_DEFAULT}

read -p "Create image via bitbake [$CREATE_IMAGE]: " CREATE_IMAGE

if [[ "$CREATE_IMAGE" == "y" ]]; then
    #Set up bitbake
    . oe-init-build-env ""
    
    #Build wic dependencies
    bitbake wic-tools
    
    #Create image
    IMAGE_PATH=$PWD/$( basename $( wic create sdimage-bootpart -e ${IMAGE_NAME} 2>&1 | tee /dev/tty | grep "INFO: The new image(s) can be found here:" -A1 | sed -n '2p' ) )
else
    read -p "Please enter absolute path of the image [$IMAGE_PATH]: " IMAGE_PATH
fi

#   Unmounting and deleting existing partitions
#   on the SD card avoids problems when writing
#   images multiple times.

# Unmount all partitions of the device
# Delete all partitions
if [[ "$OSTYPE" == "darwin"* ]]; then
    sudo diskutil unmountDisk ${SD_DEVICE}
    sudo diskutil eraseDisk FAT32 CARI MBRFormat ${SD_DEVICE}
    sudo diskutil unmountDisk ${SD_DEVICE}
else
    sudo umount ${SD_DEVICE}*
    sudo wipefs --force -a $SD_DEVICE
fi

printf "${ORANGE}Writing SD image from ${IMAGE_PATH} to $SD_DEVICE\n"
if sudo dd if=${IMAGE_PATH} of=$SD_DEVICE bs=$BLOCK_SIZE ; then
    # Wait until dd finishes
    printf "${ORANGE}Waiting for copying process to complete...\n"
    sudo sync

    printf "${GREEN}Copying process succeeded.\n${NC}"
else
    printf "${RED}Copying process failed!\n${NC}"
    exit 1
fi

sleep 3

# refresh device list /dev:
if [[ "$OSTYPE" == "darwin"* ]]; then
    sudo diskutil unmountDisk ${SD_DEVICE}
else
    sudo partprobe
    sudo umount ${SD_DEVICE}*
fi

sleep 3

# Figure out boot partition name:
if [ -e "${SD_DEVICE}p1" ]; then
    SD_DEVICE_BOOT_PARTITION=${SD_DEVICE}p1
elif [ -e "${SD_DEVICE}s1" ]; then
    SD_DEVICE_BOOT_PARTITION=${SD_DEVICE}s1
elif [ -e "${SD_DEVICE}1" ]; then
    SD_DEVICE_BOOT_PARTITION=${SD_DEVICE}1
else
    printf "${RED}Could not find boot partition on device ${SD_DEVICE}.\n${NC}"
    exit 1
fi
printf "${ORANGE}Found boot partition at ${SD_DEVICE_BOOT_PARTITION}${NC}\n"

# Assign MAC address
mkdir -p cari_boot
if [[ "$OSTYPE" == "darwin"* ]]; then
    sudo diskutil mount -mountPoint cari_boot ${SD_DEVICE_BOOT_PARTITION} || sudo diskutil mount -mountPoint cari_boot ${SD_DEVICE_BOOT_PARTITION}
else
    sudo mount -o umask=000  ${SD_DEVICE_BOOT_PARTITION} cari_boot
fi

# copy the correct uEnv.txt file
cp $SCRIPT_RELATIVE_DIR/uEnv.txt cari_boot/uEnv.txt

# Figure out the MAC address
case "$DEVICE_NAME" in
    "pclcd-lab-zynq"       ) mac="00:0A:35:00:02:00";;
    "pclcd-testbeam-zynq"  ) mac="00:0A:35:00:02:01";;
    "pclcd-zynq"[0-9]      ) mac="00:0A:35:00:03:0${DEVICE_NAME: -1}";;
    "pclcd-zynq"[1-9][0-9] ) mac="00:0A:35:00:03:${DEVICE_NAME: -2}";;
    *                 ) printf "${RED}Unknown device name. Can't assign the MAC address !\n";
			printf "${NC}\n";
			rm -R cari_boot;
			rm $IMAGE_PATH;
			exit 1;;
esac

echo "ethaddr=${mac}" >> cari_boot/uEnv.txt

# Make sure SD is not busy when unmounting:
sudo sync
sleep 3
if [[ "$OSTYPE" == "darwin"* ]]; then
    sudo diskutil unmountDisk ${SD_DEVICE}
else
    sudo umount cari_boot
fi

rm -R cari_boot

if [[ "$CREATE_IMAGE" == "y" ]]; then
    rm $IMAGE_PATH
fi

printf "${GREEN}Finished SD card preparation."
printf "${NC}\n"
